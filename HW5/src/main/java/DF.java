import org.apache.spark.api.java.function.FlatMapGroupsWithStateFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.*;

import java.util.*;

import static org.apache.spark.sql.functions.*;

class TimeComparator implements Comparator<Event>
{
    @Override
    public int compare(Event o1, Event o2) {
        return o1.getStart().compareTo(o2.getStart());
    }
}

public class DF {
    public static void main(String[] args) throws StreamingQueryException {
        SparkSession spark = SparkSession.builder().master("local").appName("Streaming Kafka").getOrCreate();
        Dataset<Row> lines = spark
                .readStream()
                .format("kafka")
                .option("kafka.bootstrap.servers", "localhost:9092")
                .option("subscribe", "test")
                .load();
        spark.sparkContext().setLogLevel("ERROR");

        Dataset<Row> message = lines.selectExpr("CAST(value AS STRING)", "CAST(key AS STRING)")
                .withColumn("_tmp", split(col("value"),","))
                .select(col("key").as("name"),
                        col("_tmp").getItem(0).as("action"),
                        col("_tmp").getItem(1).cast("TIMESTAMP").as("start"))
                .drop("_tmp");

        Dataset<Event> stateIn = message.as(Encoders.bean(Event.class));
        Dataset<Event> userActDS = stateIn.withWatermark("start", "2 minutes");


        FlatMapGroupsWithStateFunction<String, Event, SessionInfo, SessionState> stateUpdate =
                new FlatMapGroupsWithStateFunction<String, Event, SessionInfo, SessionState>() {
                    @Override
                    public Iterator<SessionState> call(String name, Iterator<Event> events, GroupState<SessionInfo> state) throws Exception {
                        List<SessionState> output = new ArrayList<>();
                        List<Event> eventList = new ArrayList<>();
                        while (events.hasNext()) {
                            eventList.add(events.next());
                        }
                        eventList.sort(new TimeComparator());
                        if (state.exists()){
                            SessionInfo oldState = state.get();
                            output.add(new SessionState(oldState.getName(), oldState.getAction(), oldState.getStart(), eventList.get(0).getStart()));
                        }
                        for (int i = 0; i < eventList.size()-1; i++) {
                            output.add(new SessionState(eventList.get(i).getName(), eventList.get(i).getAction(), eventList.get(i).getStart(), eventList.get(i+1).getStart()));
                        }
                        Event lastEvent = eventList.get(eventList.size()-1);
                        output.add(new SessionState(lastEvent.getName(), lastEvent.getAction(), lastEvent.getStart(), null));
                        state.update(new SessionInfo(lastEvent.getName(), lastEvent.getAction(), lastEvent.getStart(), null));
                        return output.iterator();
                    }
                };

        Dataset<SessionState> userAct = userActDS.groupByKey(new MapFunction<Event, String>() {
            @Override
            public String call(Event userStateIn) throws Exception {
                return userStateIn.getName();
            }
        }, Encoders.STRING())
                .flatMapGroupsWithState(stateUpdate, OutputMode.Update(), Encoders.bean(SessionInfo.class), Encoders.bean(SessionState.class), GroupStateTimeout.EventTimeTimeout());

        userAct.select("name", "action","start", "end")
                .writeStream()
                .outputMode("update")
                .format("console")
                .option("truncate","false")
                .start();


        message.groupBy(col("name"), window(col("start"), "5 minutes", "5 minutes").as("time"))
                .count().as("actionAmount")
                .writeStream()
                .outputMode("complete")
                .format("console")
                .option("truncate", "false")
                .start()
                .awaitTermination();

        spark.stop();

    }
}
