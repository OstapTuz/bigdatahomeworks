import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.sql.Timestamp;
import java.util.Properties;

import java.util.Scanner;

public class ProducerCreator {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer producer = new KafkaProducer<String,String>(properties);

        Scanner scanner = new Scanner(System.in);
        String name = "";
        String action = "";
        while (true)
        {
            System.out.println("Enter name:");
            name = scanner.nextLine();
            System.out.println("Enter action:");
            action = scanner.nextLine();
            ProducerRecord<String,String>record = new ProducerRecord<String, String>("test", name, action+","+new Timestamp(System.currentTimeMillis()).toString());

            try{
                producer.send(record);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }
}
