import java.io.Serializable;
import java.sql.Timestamp;

public class SessionState implements Serializable {
    private String name;
    private String action;
    private Timestamp start;
    private Timestamp end;

    public SessionState(){}

    public SessionState(String name, String action, Timestamp start, Timestamp end) {
        this.name = name;
        this.action = action;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "SessionState{" +
                "name='" + name + '\'' +
                ", action='" + action + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}
