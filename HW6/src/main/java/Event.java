import java.io.Serializable;
import java.sql.Timestamp;

public class Event implements Serializable {
    private String name;
    private String action;
    private Timestamp start;

    public Event(){}

    public Event(String name, String action, Timestamp start) {
        this.name = name;
        this.action = action;
        this.start = start;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", action='" + action + '\'' +
                ", start=" + start +
                '}';
    }
}
