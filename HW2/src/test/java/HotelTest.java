import demo.Hotel;
import demo.MyMapper;
import demo.MyReducer;
import demo.keyValue.BookingKey;
import demo.keyValue.BookingVaue;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HotelTest {
    MapDriver<Object, Text, BookingKey, BookingVaue> mapDriver;
    ReduceDriver<BookingKey, BookingVaue, BookingKey, BookingVaue> reduceDriver;
    MapReduceDriver<Object, Text, BookingKey, BookingVaue, BookingKey, BookingVaue> mapReduceDriver;

    @Before
    public void setUp() {
        MyMapper myMapper = new MyMapper();
        mapDriver = new MapDriver<Object, Text, BookingKey, BookingVaue>();
        mapDriver.setMapper(myMapper);

        MyReducer myReducer = new MyReducer();
        reduceDriver = new ReduceDriver<BookingKey, BookingVaue, BookingKey, BookingVaue>();
        reduceDriver.setReducer(myReducer);

        mapReduceDriver = new MapReduceDriver<Object, Text, BookingKey, BookingVaue, BookingKey, BookingVaue>();
        mapReduceDriver.setMapper(myMapper);
        mapReduceDriver.setReducer(myReducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new Object(), new Text("18,2015-08-18 12:37:10,2,3,57,342,5021,null,57,0,0,5,2017-09-19,2017-09-20,2,0,1,8243,1,2826088480774"));

        BookingKey bookingKey = new BookingKey();
        bookingKey.setId("18");
        bookingKey.setSrchSi("2017-09-19");
        bookingKey.setHotelId("2826088480774");

        BookingVaue bookingVaue = new BookingVaue(2,3,57,342,5021,null,57,5,2,0,1,8243,1);
        bookingVaue.setDateTime("2015-08-18 12:37:10");
        bookingVaue.setMobile("0");
        bookingVaue.setPackage("0");
        bookingVaue.setSrchCo("2017-09-20");

       mapDriver.withOutput(bookingKey,bookingVaue);
       mapDriver.runTest();
    }

    @Test
    public void testReducer() throws IOException {
        BookingKey bookingKey = new BookingKey();
        bookingKey.setId("18");
        bookingKey.setSrchSi("2017-09-19");
        bookingKey.setHotelId("2826088480774");

        BookingVaue bookingVaue = new BookingVaue(2,3,57,342,5021,null,57,5,2,0,1,8243,1);
        bookingVaue.setDateTime("2015-08-18 12:37:10");
        bookingVaue.setMobile("0");
        bookingVaue.setPackage("0");
        bookingVaue.setSrchCo("2017-09-20");

        List<BookingVaue>bookingVaues = new ArrayList<>();
        bookingVaues.add(bookingVaue);

        reduceDriver.withInput(bookingKey, bookingVaues);
        reduceDriver.withOutput(bookingKey, bookingVaue);

        reduceDriver.runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver.withInput(new Object(), new Text("18,2015-08-18 12:37:10,2,3,57,342,5021,null,57,0,0,5,2017-09-19,2017-09-20,2,0,1,8243,1,2826088480774"));
        BookingKey bookingKey = new BookingKey();
        bookingKey.setId("18");
        bookingKey.setSrchSi("2017-09-19");
        bookingKey.setHotelId("2826088480774");

        BookingVaue bookingVaue = new BookingVaue(2,3,57,342,5021,null,57,5,2,0,1,8243,1);
        bookingVaue.setDateTime("2015-08-18 12:37:10");
        bookingVaue.setMobile("0");
        bookingVaue.setPackage("0");
        bookingVaue.setSrchCo("2017-09-20");

        mapReduceDriver.withOutput(bookingKey,bookingVaue);
        mapReduceDriver.runTest();
    }
}

