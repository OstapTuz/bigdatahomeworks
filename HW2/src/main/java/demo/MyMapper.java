package demo;

import demo.keyValue.BookingKey;
import demo.keyValue.BookingVaue;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/*
 * class mapper
 */
public class MyMapper extends Mapper<Object, Text, BookingKey, BookingVaue> {
    @Override
    protected void map(Object key, Text value, Mapper.Context context) throws IOException, InterruptedException {
        /*
         *parses text into lines and every line into objects
         */
        String text = value.toString();
        String[] lines = text.split("\n");

        BookingKey bookingKey = null;
        BookingVaue bookingVaue = null;
        for (String strElement : lines) {
            String[] element = strElement.split(",");
            /*
             *create composite key
             * create value object
             */
            bookingKey = new BookingKey();
            bookingVaue = new BookingVaue();

            bookingKey.setId(element[0]);
            bookingKey.setSrchSi(element[12]);
            bookingKey.setHotelId(element[19]);

            bookingVaue.setDateTime(element[1]);
            bookingVaue.setSiteName(element[2]);
            bookingVaue.setPosaContinent(element[3]);
            bookingVaue.setUserLocationCountry(element[4]);
            bookingVaue.setUserLocationRegion(element[5]);
            bookingVaue.setUserLocationCity(element[6]);
            bookingVaue.setOrigDestinationDistance(element[7]);
            bookingVaue.setUserId(element[8]);
            bookingVaue.setMobile(element[9]);
            bookingVaue.setPackage(element[10]);
            bookingVaue.setChannel(element[11]);
            bookingVaue.setSrchCo(element[13]);
            bookingVaue.setSrchAdultsCount(element[14]);
            bookingVaue.setSrchChildrenCount(element[15]);
            bookingVaue.setSrchRmCount(element[16]);
            bookingVaue.setSrchDestinationId(element[17]);
            bookingVaue.setSrchDectinationTypeId(element[18]);

            /*
             *check if variable srchAdultsCount is more than 2
             * if true write pair key-value into context
             */
            if (bookingVaue.getSrchAdultsCount() >= 2) {
                context.write(bookingKey, bookingVaue);
            }
        }
    }
}
