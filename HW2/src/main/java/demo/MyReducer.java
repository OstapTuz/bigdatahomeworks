package demo;

import demo.keyValue.BookingKey;
import demo.keyValue.BookingVaue;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/*
 *class reducer
 */
public class MyReducer extends Reducer<BookingKey, BookingVaue, BookingKey, BookingVaue> {
    @Override
    protected void reduce(BookingKey key, Iterable<BookingVaue> values, Context context) throws IOException, InterruptedException {
        for (BookingVaue value : values) {
            context.write(key, value);
        }
    }
}
