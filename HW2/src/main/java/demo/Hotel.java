package demo;

import demo.comparator.BookingComparator;
import demo.comparator.BookingPartitioner;
import demo.comparator.GroupComparator;
import demo.keyValue.BookingKey;
import demo.keyValue.BookingVaue;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/*
 * configuration class to create and configure job
 */
public class Hotel {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        /*
         * create configuration and job
         */
        Configuration conf = new Configuration();
        Job job = new Job(conf, "sort_job");
        /*
         * set mapper and reducer classes
         */
        job.setJarByClass(Hotel.class);
        job.setMapperClass(MyMapper.class);
        job.setReducerClass(MyReducer.class);
        /*
         * set comparator, group comparator and partitioner classes
         */
        job.setSortComparatorClass(BookingComparator.class);
        job.setGroupingComparatorClass(GroupComparator.class);
        job.setPartitionerClass(BookingPartitioner.class);
        job.setNumReduceTasks(1);

        /*
         * set map and value output keys
         */
        job.setMapOutputKeyClass(BookingKey.class);
        job.setMapOutputValueClass(BookingVaue.class);
        job.setOutputKeyClass(BookingKey.class);
        job.setOutputValueClass(BookingVaue.class);
        /*
         * set arguments from function main as a path to input and output files/directories
         */
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
