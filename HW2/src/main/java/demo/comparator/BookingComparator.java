package demo.comparator;

import demo.keyValue.BookingKey;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/*
 *class comparator
 */
public class BookingComparator extends WritableComparator {
    protected BookingComparator() {
        super(BookingKey.class, true);
    }

    /*
     *sort composite keys by hotel_id then by srhcSi and id in descending order
     */
    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        BookingKey bookingKeyA = (BookingKey) a;
        BookingKey bookingKeyB = (BookingKey) b;

        int div = bookingKeyB.getHotelId().compareTo(bookingKeyA.getHotelId());
        if (div != 0) {
            return div;
        }
        div = bookingKeyB.getSrchSi().compareTo(bookingKeyA.getSrchSi());
        if (div != 0) {
            return div;
        }
        div = bookingKeyB.getId().compareTo(bookingKeyA.getId());
        return div;
    }
}
