package demo.comparator;

import demo.keyValue.BookingKey;
import demo.keyValue.BookingVaue;
import org.apache.hadoop.mapreduce.Partitioner;

/*
 * class partitioner
 */
public class BookingPartitioner extends Partitioner<BookingKey, BookingVaue> {
    @Override
    public int getPartition(BookingKey bookingKey, BookingVaue bookingVaue, int numPartitions) {
        return bookingKey.getHotelId().hashCode() % numPartitions;
    }
}
