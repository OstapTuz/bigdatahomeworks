package demo.comparator;

import demo.keyValue.BookingKey;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/*
 * class group comparator
 */
public class GroupComparator extends WritableComparator {
    protected GroupComparator() {
        super(BookingKey.class, true);
    }

    /*
     * sorting by hotel_id
     */
    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        BookingKey bookingKeyA = (BookingKey) a;
        BookingKey bookingKeyB = (BookingKey) b;

        return bookingKeyA.getHotelId().compareTo(bookingKeyB.getHotelId());
    }
}
