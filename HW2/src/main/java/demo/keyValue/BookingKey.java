package demo.keyValue;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/*
 * class template for composite key
 */
public class BookingKey implements WritableComparable<BookingKey> {
    protected static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private Long id;
    private Long srchSi;
    private Long hotelId;

    public Long getId() {
        return id;
    }

    public void setId(String id) {
        this.id = Long.parseLong(id);
    }

    public Long getSrchSi() {
        return srchSi;
    }

    public void setSrchSi(String srchSi) {
        try {
            this.srchSi = dateFormat.parse(srchSi).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = Long.parseLong(hotelId);
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(id);
        dataOutput.writeLong(srchSi);
        dataOutput.writeLong(hotelId);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        id = dataInput.readLong();
        srchSi = dataInput.readLong();
        hotelId = dataInput.readLong();
    }

    @Override
    public int compareTo(BookingKey o) {
        int div = hotelId.compareTo(o.getHotelId());
        if (div != 0) {
            return div;
        }
        div = srchSi.compareTo(o.getSrchSi());
        if (div != 0) {
            return div;
        }
        div = id.compareTo(o.getId());
        return div;
    }

    @Override
    public String toString() {
        return "BookingKey{" +
                "id=" + id +
                ", srchSi=" + dateFormat.format(srchSi) +
                ", hotelId=" + hotelId +
                '}';
    }
}
