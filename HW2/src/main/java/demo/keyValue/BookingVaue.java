package demo.keyValue;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/*
 * class template for value object
 */
public class BookingVaue implements Writable {
    protected static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private Long dateTime;
    private Integer siteName;
    private Integer posaContinent;
    private Integer userLocationCountry;
    private Integer userLocationRegion;
    private Integer userLocationCity;
    private Double origDestinationDistance;
    private Integer userId;
    private Boolean isMobile;
    private Boolean isPackage;
    private Integer channel;
    private Long srchCo;
    private Integer srchAdultsCount;
    private Integer srchChildrenCount;
    private Integer srchRmCount;
    private Integer srchDestinationId;
    private Integer srchDectinationTypeId;

    public BookingVaue() {
    }

    public BookingVaue(Integer siteName, Integer posaContinent, Integer userLocationCountry, Integer userLocationRegion, Integer userLocationCity, Double origDestinationDistance, Integer userId, Integer channel, Integer srchAdultsCount, Integer srchChildrenCount, Integer srchRmCount, Integer srchDestinationId, Integer srchDectinationTypeId) {
        this.siteName = siteName;
        this.posaContinent = posaContinent;
        this.userLocationCountry = userLocationCountry;
        this.userLocationRegion = userLocationRegion;
        this.userLocationCity = userLocationCity;
        this.origDestinationDistance = origDestinationDistance;
        this.userId = userId;
        this.channel = channel;
        this.srchAdultsCount = srchAdultsCount;
        this.srchChildrenCount = srchChildrenCount;
        this.srchRmCount = srchRmCount;
        this.srchDestinationId = srchDestinationId;
        this.srchDectinationTypeId = srchDectinationTypeId;
    }

    public Integer getSrchAdultsCount() {
        return srchAdultsCount;
    }

    public void setSrchAdultsCount(String srchAdultsCount) {
        this.srchAdultsCount = Integer.parseInt(srchAdultsCount);
    }

    public void setDateTime(String dateTime) {
        try {
            this.dateTime = dateTimeFormat.parse(dateTime).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setSiteName(String siteName) {
        this.siteName = Integer.parseInt(siteName);
    }

    public void setPosaContinent(String posaContinent) {
        this.posaContinent = Integer.parseInt(posaContinent);
    }

    public void setUserLocationCountry(String userLocationCountry) {
        this.userLocationCountry = Integer.parseInt(userLocationCountry);
    }

    public void setUserLocationRegion(String userLocationRegion) {
        this.userLocationRegion = Integer.parseInt(userLocationRegion);
    }

    public void setUserLocationCity(String userLocationCity) {
        this.userLocationCity = Integer.parseInt(userLocationCity);
    }

    public void setOrigDestinationDistance(String origDestinationDistance) {
        this.origDestinationDistance = ("null".equals(origDestinationDistance)) ? null : Double.parseDouble(origDestinationDistance);
    }

    public void setUserId(String userId) {
        this.userId = Integer.parseInt(userId);
    }

    public void setMobile(String mobile) {
        isMobile = "1".equals(mobile);
    }

    public void setPackage(String aPackage) {
        isPackage = "1".equals(aPackage);
    }

    public void setChannel(String channel) {
        this.channel = Integer.parseInt(channel);
    }

    public void setSrchCo(String srchCo) {
        try {
            this.srchCo = dateFormat.parse(srchCo).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setSrchChildrenCount(String srchChildrenCount) {
        this.srchChildrenCount = Integer.parseInt(srchChildrenCount);
    }

    public void setSrchRmCount(String srchRmCount) {
        this.srchRmCount = Integer.parseInt(srchRmCount);
    }

    public void setSrchDestinationId(String srchDestinationId) {
        this.srchDestinationId = Integer.parseInt(srchDestinationId);
    }

    public void setSrchDectinationTypeId(String srchDectinationTypeId) {
        this.srchDectinationTypeId = Integer.parseInt(srchDectinationTypeId);
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(dateTime);
        dataOutput.writeInt(siteName);
        dataOutput.writeInt(posaContinent);
        dataOutput.writeInt(userLocationCountry);
        dataOutput.writeInt(userLocationRegion);
        dataOutput.writeInt(userLocationCity);
        if (origDestinationDistance != null) {
            dataOutput.writeBoolean(true);
            dataOutput.writeDouble(origDestinationDistance);
        } else {
            dataOutput.writeBoolean(false);
        }
        dataOutput.writeInt(userId);
        dataOutput.writeBoolean(isMobile);
        dataOutput.writeBoolean(isPackage);
        dataOutput.writeInt(channel);
        dataOutput.writeLong(srchCo);
        dataOutput.writeInt(srchAdultsCount);
        dataOutput.writeInt(srchChildrenCount);
        dataOutput.writeInt(srchRmCount);
        dataOutput.writeInt(srchDestinationId);
        dataOutput.writeInt(srchDectinationTypeId);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        dateTime = dataInput.readLong();
        siteName = dataInput.readInt();
        posaContinent = dataInput.readInt();
        userLocationCountry = dataInput.readInt();
        userLocationRegion = dataInput.readInt();
        userLocationCity = dataInput.readInt();
        if (dataInput.readBoolean()) {
            origDestinationDistance = dataInput.readDouble();
        } else {
            origDestinationDistance = null;
        }
        userId = dataInput.readInt();
        isMobile = dataInput.readBoolean();
        isPackage = dataInput.readBoolean();
        channel = dataInput.readInt();
        srchCo = dataInput.readLong();
        srchAdultsCount = dataInput.readInt();
        srchChildrenCount = dataInput.readInt();
        srchRmCount = dataInput.readInt();
        srchDestinationId = dataInput.readInt();
        srchDectinationTypeId = dataInput.readInt();
    }

    @Override
    public String toString() {
        return "BookingVaue{" +
                "dateTime=" + dateTimeFormat.format(dateTime) +
                ", siteName=" + siteName +
                ", posaContinent=" + posaContinent +
                ", userLocationCountry=" + userLocationCountry +
                ", userLocationRegion=" + userLocationRegion +
                ", userLocationCity=" + userLocationCity +
                ", origDestinationDistance=" + origDestinationDistance +
                ", userId=" + userId +
                ", isMobile=" + isMobile +
                ", isPackage=" + isPackage +
                ", channel=" + channel +
                ", srchCo=" + dateFormat.format(srchCo) +
                ", srchAdultsCount=" + srchAdultsCount +
                ", srchChildrenCount=" + srchChildrenCount +
                ", srchRmCount=" + srchRmCount +
                ", srchDestinationId=" + srchDestinationId +
                ", srchDectinationTypeId=" + srchDectinationTypeId +
                '}';
    }
}
