package demo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class WordCounter {
        public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
            Configuration configuration = new Configuration();
            String[] files = new GenericOptionsParser(configuration, args).getRemainingArgs();
            Path input = new Path(files[0]);
            Path output = new Path(files[1]);
            Job job = new Job(configuration, "wordcounter");
            job.setJarByClass(WordCounter.class);
            job.setMapperClass(MapForWordCount.class);
            job.setReducerClass(ReduceForWordCounter.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(IntWritable.class);

            FileInputFormat.addInputPath(job, input);
            FileOutputFormat.setOutputPath(job, output);
            System.exit(job.waitForCompletion(true)?0:1);
    }

public static class MapForWordCount extends Mapper<Object, Text, Text, IntWritable>{
    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String text = value.toString();
        List<String> words = Arrays.asList(text.split("[(\\.*\\s)(,\\s)\\s*(!*\\s)(?*\\s)\\.*!*?*]"));

        for (String word : words){
            Text outputKey = new Text(word.trim().toUpperCase());
            context.write(outputKey, new IntWritable(1));
        }
    }
}

public static class ReduceForWordCounter extends Reducer<Object, IntWritable, Text, IntWritable>{
    @Override
    protected void reduce(Object key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable value: values) {
            sum += value.get();
        }
        context.write((Text)key, new IntWritable(sum));
    }
}
}
