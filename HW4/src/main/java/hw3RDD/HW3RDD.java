package hw3RDD;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class HW3RDD {
    public static void main(String[] args) {
        final SparkConf conf = new SparkConf().setAppName("hw3RDD");
        final JavaSparkContext context = new JavaSparkContext(conf);

        final JavaRDD<String> goalies = context.textFile(args[0]);  //goalies.csv
        final JavaRDD<String> teams = context.textFile(args[1]); //teams.csv
        final JavaRDD<String> players = context.textFile(args[2]); //master.csv

        String goaliesHeader = goalies.first();

        final JavaPairRDD<String, Long> allPlayerIdAndGoalies = goalies.filter(line -> !line.equals(goaliesHeader)).mapToPair(line -> //id гравців і 1
        {
            String[] values = line.split(",",-1);
            Long goals = 0L;
            if (!"".equals(values[21]))
            {
                goals = Long.parseLong(values[21]);
            }
            return new Tuple2<>(values[0], goals);
        });


        final JavaPairRDD<String, String> allTeamIdAndTeamName = teams.mapToPair(line -> //id команди і назва
        {
            String[] values = line.split(",");
            return new Tuple2<>(values[2], values[18]);
        });

        final JavaPairRDD<String, String> allPlayerIdAndTeamId = goalies.mapToPair(line -> //id команди і id гравця
        {
            String[] values = line.split(",");
            return new Tuple2<>(values[3], values[0]);
        });

        final JavaPairRDD<String, Tuple2<String, String>> allPlayerIdAndTeamNameJoined = allPlayerIdAndTeamId.join(allTeamIdAndTeamName);
        final JavaPairRDD<String, String> allPlayerIdAndTeamName = allPlayerIdAndTeamNameJoined.mapToPair(x -> new Tuple2<>(x._2._1, x._2._2)).distinct(); //id гравця і команда

        final JavaPairRDD<String, String> playerIdAndFullName = players.mapToPair(line -> //id гравця і повне ім'я
        {
            String[] values = line.split(",");
            String playerId = values[0];
            if (("".equals(playerId))) {
               playerId = "unknown";
            }
            return new Tuple2<>(playerId, String.join(" ", values[3], values[4]));
        });

        final JavaPairRDD<String, Long> prePlayerIdAndGoalies = allPlayerIdAndGoalies.reduceByKey((x1, x2) -> x1 + x2); //id гравця і сума голів

        final List<Tuple2<Long, String>> playerIdAndGoaliesList = prePlayerIdAndGoalies
                .mapToPair(x -> new Tuple2<Long, String>(x._2, x._1))
                .sortByKey(false)
               .take(10);
        final JavaRDD playerIdAndGoaliesRDD = context.parallelize(playerIdAndGoaliesList);
        final JavaPairRDD<Long, String> playerIdAndGoaliesDesc = JavaPairRDD
                .fromJavaRDD(playerIdAndGoaliesRDD);
        final JavaPairRDD<String, Long> playerIdAndGoalies = playerIdAndGoaliesDesc.mapToPair(x -> new Tuple2<String, Long>(x._2, x._1));
        final JavaPairRDD<String, Iterable<String>> playerIdAndTeamId = allPlayerIdAndTeamName.groupByKey(); //id гравця і список його команд
        final JavaPairRDD<String, Tuple2<Tuple2<String, Long>, Iterable<String>>> preResult = playerIdAndFullName.join(playerIdAndGoalies).join(playerIdAndTeamId); //join
        final JavaPairRDD<String, String> resultRDD = preResult.mapToPair(line -> new Tuple2<>(line._2._1._1, String.join(",", line._2._1._2.toString(), line._2._2.toString())));

        resultRDD.repartition(2).saveAsTextFile(args[3]); // output directory

        context.stop();
    }
}
