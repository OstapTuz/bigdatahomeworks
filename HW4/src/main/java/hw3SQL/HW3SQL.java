package hw3SQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;


public class HW3SQL {
    public static void main(String[] args) {
        SparkSession session = SparkSession.builder()
                .appName("HW3SQL")
                .getOrCreate();

        Dataset<Row> goalies =  session.read().option("header", "true").csv(args[0]); //goalies.csv
        goalies.createOrReplaceTempView("goalies");
        Dataset<Row> master = session.read().option("header", "true").csv(args[2]); //master.csv
        master.createOrReplaceTempView("master");
        Dataset<Row> teams = session.read().option("header", "true").csv(args[1]); //teams.csv
        teams.createOrReplaceTempView("teams");

        Dataset<Row>groupGoalies = session.sql("select distinct playerID, sum(PostGA) as goaliesAmount from goalies group by playerID");
        groupGoalies.createOrReplaceTempView("groupGoalies");

        Dataset<Row>preResult = session
                .sql("select g.playerID, concat(master.firstName,' ', master.lastName)as playerName, collect_list(t.name) as teams " +
                        "from (select distinct goalies.playerID, goalies.tmID from goalies) g inner join master inner join (select distinct teams.tmID, teams.name from teams) t " +
                        "on g.playerID = master.playerID and g.tmID = t.tmID " +
                        "group by g.playerID, playerName");
        preResult.createOrReplaceTempView("namesAndTeams");

        Dataset<Row> result = session.sql("select namesAndTeams.playerName, groupGoalies.goaliesAmount, namesAndTeams.teams " +
                "from namesAndTeams inner join groupGoalies on groupGoalies.playerID = namesAndTeams.playerID " +
                "order by goaliesAmount desc limit 10");

        result.repartition(2).javaRDD().saveAsTextFile(args[3]); //output directory
        session.stop();
    }
}
