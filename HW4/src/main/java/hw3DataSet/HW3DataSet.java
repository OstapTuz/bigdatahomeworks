package hw3DataSet;

import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.*;

public class HW3DataSet {
    public static void main(String[] args) {
        SparkSession session = SparkSession
                .builder()
                .appName("HW3DataSet")
                .getOrCreate();

        Dataset<Row>teamNames = session.read().csv(args[1]) //teams.csv
                .select(col("_c2").as("teamId"), col("_c18").as("teamName"));

        Dataset<Row>playerIdAndNames = session.read().csv(args[2]) //master.csv
                .select(col("_c0").as("playerId"), col("_c3").as("name"), col("_c4").as("surname"));

        Dataset<Row>playerIdAndTeamId = session.read().csv(args[0]) //goalies.csv
                .select(col("_c0").as("playerId"), col("_c3").as("teamId"))
                .join(teamNames, "teamId")
                .distinct()
                .groupBy("playerId")
                .agg(collect_list("teamName").as("teams"));

        Dataset<Row> playerIdAndGoaliesAmount = session.read().csv(args[0]) //goalies.csv
                .select(col("_c0").as("playerId"),col("_c21").as("goals"))
                .join(playerIdAndNames, "playerId").select(col("playerId"),col("goals"),functions.concat(col("name"), lit(" "),col("surname")).as("playerName"))
                .groupBy("playerId", "playerName")
                .agg(sum("goals").as("goaliesAmount"));

        Dataset<Row> result = playerIdAndGoaliesAmount
                .join(playerIdAndTeamId, "playerId")
                .select(col("playerName"), col("goaliesAmount"), col("teams"))
                .orderBy(desc("goaliesAmount")).limit(10);

        result.repartition(2).javaRDD().map(x -> x.toString()).saveAsTextFile(args[3]); // output  directory
        session.stop();
    }
}
